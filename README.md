# DeppGPT-API and Command-Line-Interface

Originally from [der postillion](https://www.der-postillon.com/2023/05/deppgpt.html) (german satire magazine)<br>

The first and only real human artificial intelligence<br>
DeppGPT gives no sensible answers and insults everything and everyone

### CLI
```bash
$node main.mjs
```

### Core
```js
var APIClient = require("../deppgpt-api/core/client.mjs");
// APIClient.fetch(text, messages = [], maxMessages = 100); // Promise<string>

var client = new APIClient();
// client.fetch(text, messages, maxMessages); // Promise<string>

```