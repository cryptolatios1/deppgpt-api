import https from "https";

let baseUrl = "https://europe-west1-deppgpt.cloudfunctions.net/DeppGPTRelease221/";

export class Client {
	constructor(messages = [], maxMessages = 100, cacheMessages = true) {
		this.messages = messages;
		this.cacheMessages = cacheMessages;
		this.maxMessages = maxMessages;
	};

	/**@param {string} text @param {{role: "user"|"assistant", content: string}[]} messages @param {number} maxMessages @return {Promise<{error: string}|{answer: string, totalTokens: number}>}*/
	static fetch(text, messages = [], maxMessages = 100) {
		let body = JSON.stringify({"messages": [...messages, {"role":"user","content": text}], "maxMessages": maxMessages});
		let headers = {
			"Content-Length": Buffer.byteLength(body),
			"Content-Type": "application/json",
		};

		return new Promise((resolve, reject) => {
			https.request(baseUrl, {headers: headers, method: "POST"}, async msg => {
				let content = "";
				msg.setEncoding("utf8");
				for await (let body of msg) content += body;
				let result;
				try {
					result = JSON.parse(content);
				} catch {
					result = {error: content};
				}
				resolve(result);
			}).on("error", reject).end(body);
		});
	};

	/**@param {string} text @param {{role: "user"|"assistant", content: string}[]} messages @param {number} maxMessages @return {Promise<{error: string}|{answer: string, totalTokens: number}>}*/
	async fetch(text, messages = this.cacheMessages ? this.messages : [], maxMessages = this.maxMessages) {
		let result = await Client.fetch(text, messages, maxMessages);
		if(typeof result.answer === "string" && this.cacheMessages) {
			this.messages.push({role: "user", content: text}, {role: "assistant", content: result.answer});
		};
		return result;
	};

	clearMessages() {
		this.messages = [];
	};
};