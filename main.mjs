import readline from "readline";
import { Client } from "./core/client.mjs";

let client = new Client([], 10, true);
readline.createInterface({
	input: process.stdin,
	output: process.stdout
}).on("line", async function(data) {
	data = data.trim();
	if(data.startsWith("/clear")) {
		console.clear();
		client.clearMessages();
	} else {
		let result = await client.fetch(data);
		process.stdout.write("\x1b[30m[DeppGPT]:\x1b[0m " + (result.answer ?? result.error) + "\n");
	}
});